# Lab5 -- Integration testing

Andrey Khoroshavin, B19-CS-01

## Introduction

From the initial information, the following service address was given to me by teaching stuff - https://script.google.com/macros/s/AKfycbx8vetYY9K8yM3a46_mZJX2kMgbRgOEBb1_FlgYlMsOuPM5C5aw0FwTZDQ3StsjO1J0Wg/exec

The default values for my replica are:
```
Here is InnoCar Specs:
Budet car price per minute = 15
Luxury car price per minute = 37
Fixed price per km = 14
Allowed deviations in % = 6
Inno discount in % = 5
```
This information is available by the request: https://script.google.com/macros/s/AKfycbx8vetYY9K8yM3a46_mZJX2kMgbRgOEBb1_FlgYlMsOuPM5C5aw0FwTZDQ3StsjO1J0Wg/exec?email=a.khoroshavin@innopolis.university&service=getSpec

## Boundary Value Analysis

For the service `calculatePrice` there are 7 variables with the following equivalent classes:
- `plan` \
    |-> `minute` \
    |-> `fixed_price` \
    |-> NaN \
    '-> others (nonsense)

- `type` \
    |-> `budget` \
    |-> `luxury` \
    |-> NaN \
    '-> others (nonsense)

- `distance` \
    |-> greater than 0 \
    |-> less or equal to 0 \
    |-> NaN \
    '-> is not a number (nonsense)

- `planned_distance` \
    |-> greater than 0 \
    |-> less or equal to 0 \
    |-> NaN \
    '-> is not a number (nonsense)

- `time` \
    |-> greater than 0 \
    |-> less or equal to 0 \
    |-> NaN \
    '-> is not a number (nonsense)

- `planned_time` \
    |-> greater than 0 \
    |-> less or equal to 0 \
    |-> NaN \
    '-> is not a number (nonsense)

- `inno_discount` \
    |-> `yes` \
    |-> `no` \
    | -> NaN \
    '-> others (nonsense)

> `NaN` means the absence of the field in the request body 

## Decision table

The decision table is separated by sections for convenient separation of tested functionality

| #  | plan        | type     | inno_discount | distance | planned_distance | time     | planned_time | Expected result |
|----|-------------|----------|---------------|----------|------------------|----------|--------------|-----------------|
| **Validation of necessary arguments**
| 1  | NaN         | *        | *             | *        | *                | *        | *            | Invalid Request |
| 2  | nonsense    | *        | *             | *        | *                | *        | *            | Invalid Request |
| 3  | *           | NaN      | *             | *        | *                | *        | *            | Invalid Request |
| 4  | *           | nonsense | *             | *        | *                | *        | *            | Invalid Request |
| 5  | *           | *        | NaN           | *        | *                | *        | *            | Invalid Request |
| 6  | *           | *        | nonsense      | *        | *                | *        | *            | Invalid Request |
| 7  | *           | *        | *             | *        | *                | NaN      | *            | Invalid Request |
| 8  | *           | *        | *             | *        | *                | <= 0     | *            | Invalid Request |
| 9  | *           | *        | *             | *        | *                | nonsense | *            | Invalid Request |
| **Validation for fixed_prise tariff**
| 10 | fixed_prise | luxury   | *             | *        | *                | *        | *            | Invalid Request |
| 11 | fixed_prise | *        | *             | NaN      | *                | *        | *            | Invalid Request |
| 12 | fixed_prise | *        | *             | <= 0     | *                | *        | *            | Invalid Request |
| 13 | fixed_prise | *        | *             | nonsense | *                | *        | *            | Invalid Request |
| 14 | fixed_prise | *        | *             | *        | NaN              | *        | *            | Invalid Request |
| 15 | fixed_prise | *        | *             | *        | <= 0             | *        | *            | Invalid Request |
| 16 | fixed_prise | *        | *             | *        | nonsense         | *        | *            | Invalid Request |
| 17 | fixed_prise | *        | *             | *        | *                | *        | NaN          | Invalid Request |
| 18 | fixed_prise | *        | *             | *        | *                | *        | <= 0         | Invalid Request |
| 19 | fixed_prise | *        | *             | *        | *                | *        | nonsense     | Invalid Request |
| **Correctness of calculations**
| 20 | minute      | budget   | yes           | *        | *                | > 0      | *            | Correct price   |
| 21 | minute      | budget   | no            | *        | *                | > 0      | *            | Correct price   |
| 22 | minute      | luxury   | yes           | *        | *                | > 0      | *            | Correct price   |
| 23 | minute      | luxury   | no            | *        | *                | > 0      | *            | Correct price   |
| 24 | fixed_prise | budget   | yes           | > 0      | > 0              | > 0      | > 0          | Correct price   |
| 25 | fixed_prise | budget   | no            | > 0      | > 0              | > 0      | > 0          | Correct price   |
| **Switch from fixed_prise tariff to minute**
| 26 | fixed_prise | budget   | no            | 94       | 100              | 100      | 94           | Correct price   |
| 27 | fixed_prise | budget   | yes           | 93       | 100              | 100      | 100          | Correct price   |
| 28 | fixed_prise | budget   | no            | 100      | 100              | 100      | 93           | Correct price   |

## Test run

| #  | Request body | Expected result | Actual result | Test passed |
|----|--------------|-----------------|---------------|-------------|
| 1  | https://script.google.com/macros/s/AKfycbx8vetYY9K8yM3a46_mZJX2kMgbRgOEBb1_FlgYlMsOuPM5C5aw0FwTZDQ3StsjO1J0Wg/exec?email=a.khoroshavin@innopolis.university&service=calculatePrice&type=budget&inno_discount=yes&distance=1&planned_distance=1&time=1&planned_time=1 | Invalid Request | Invalid Request | ✅ |
| 2  | https://script.google.com/macros/s/AKfycbx8vetYY9K8yM3a46_mZJX2kMgbRgOEBb1_FlgYlMsOuPM5C5aw0FwTZDQ3StsjO1J0Wg/exec?email=a.khoroshavin@innopolis.university&service=calculatePrice&plan=abc&type=budget&inno_discount=yes&distance=1&planned_distance=1&time=1&planned_time=1 | Invalid Request | Invalid Request | ✅ |
| 3  | https://script.google.com/macros/s/AKfycbx8vetYY9K8yM3a46_mZJX2kMgbRgOEBb1_FlgYlMsOuPM5C5aw0FwTZDQ3StsjO1J0Wg/exec?email=a.khoroshavin@innopolis.university&service=calculatePrice&plan=minute&inno_discount=yes&distance=1&planned_distance=1&time=1&planned_time=1 | Invalid Request | Invalid Request | ✅ |
| 4  | https://script.google.com/macros/s/AKfycbx8vetYY9K8yM3a46_mZJX2kMgbRgOEBb1_FlgYlMsOuPM5C5aw0FwTZDQ3StsjO1J0Wg/exec?email=a.khoroshavin@innopolis.university&service=calculatePrice&plan=minute&type=abc&inno_discount=yes&distance=1&planned_distance=1&time=1&planned_time=1 | Invalid Request | Invalid Request | ✅ |
| 5  | https://script.google.com/macros/s/AKfycbx8vetYY9K8yM3a46_mZJX2kMgbRgOEBb1_FlgYlMsOuPM5C5aw0FwTZDQ3StsjO1J0Wg/exec?email=a.khoroshavin@innopolis.university&service=calculatePrice&plan=minute&type=budget&distance=1&planned_distance=1&time=1&planned_time=1 | Invalid Request | Invalid Request | ✅ |
| 6  | https://script.google.com/macros/s/AKfycbx8vetYY9K8yM3a46_mZJX2kMgbRgOEBb1_FlgYlMsOuPM5C5aw0FwTZDQ3StsjO1J0Wg/exec?email=a.khoroshavin@innopolis.university&service=calculatePrice&plan=minute&type=budget&inno_discount=abc&distance=1&planned_distance=1&time=1&planned_time=1 | Invalid Request | Invalid Request | ✅ |
| 7  | https://script.google.com/macros/s/AKfycbx8vetYY9K8yM3a46_mZJX2kMgbRgOEBb1_FlgYlMsOuPM5C5aw0FwTZDQ3StsjO1J0Wg/exec?email=a.khoroshavin@innopolis.university&service=calculatePrice&plan=minute&type=budget&inno_discount=yes&distance=1&planned_distance=1&planned_time=1 | Invalid Request | {"price":null} | ❌ |
| 8  | https://script.google.com/macros/s/AKfycbx8vetYY9K8yM3a46_mZJX2kMgbRgOEBb1_FlgYlMsOuPM5C5aw0FwTZDQ3StsjO1J0Wg/exec?email=a.khoroshavin@innopolis.university&service=calculatePrice&plan=minute&type=budget&inno_discount=yes&time=0 | Invalid Request | {"price":0} | ❌ |
| 9  | https://script.google.com/macros/s/AKfycbx8vetYY9K8yM3a46_mZJX2kMgbRgOEBb1_FlgYlMsOuPM5C5aw0FwTZDQ3StsjO1J0Wg/exec?email=a.khoroshavin@innopolis.university&service=calculatePrice&plan=minute&type=budget&inno_discount=yes&distance=1&planned_distance=1&time=abc&planned_time=1 | Invalid Request | {"price":null} | ❌ |
| 10 | https://script.google.com/macros/s/AKfycbx8vetYY9K8yM3a46_mZJX2kMgbRgOEBb1_FlgYlMsOuPM5C5aw0FwTZDQ3StsjO1J0Wg/exec?email=a.khoroshavin@innopolis.university&service=calculatePrice&plan=fixed_price&type=luxury&inno_discount=yes&distance=1&planned_distance=1&time=1&planned_time=1 | Invalid Request | {"price":12} | ❌ |
| 11 | https://script.google.com/macros/s/AKfycbx8vetYY9K8yM3a46_mZJX2kMgbRgOEBb1_FlgYlMsOuPM5C5aw0FwTZDQ3StsjO1J0Wg/exec?email=a.khoroshavin@innopolis.university&service=calculatePrice&plan=fixed_price&type=budget&inno_discount=yes&planned_distance=1&time=1&planned_time=1 | Invalid Request | {"price":16} | ❌ |
| 12 | https://script.google.com/macros/s/AKfycbx8vetYY9K8yM3a46_mZJX2kMgbRgOEBb1_FlgYlMsOuPM5C5aw0FwTZDQ3StsjO1J0Wg/exec?email=a.khoroshavin@innopolis.university&service=calculatePrice&plan=fixed_price&type=budget&inno_discount=yes&distance=-1&planned_distance=1&time=1&planned_time=1 | Invalid Request | {"price":16} | ❌ |
| 13 | https://script.google.com/macros/s/AKfycbx8vetYY9K8yM3a46_mZJX2kMgbRgOEBb1_FlgYlMsOuPM5C5aw0FwTZDQ3StsjO1J0Wg/exec?email=a.khoroshavin@innopolis.university&service=calculatePrice&plan=fixed_price&type=budget&inno_discount=yes&distance=abc&planned_distance=1&time=1&planned_time=1 | Invalid Request | {"price":16} | ❌ |
| 14 | https://script.google.com/macros/s/AKfycbx8vetYY9K8yM3a46_mZJX2kMgbRgOEBb1_FlgYlMsOuPM5C5aw0FwTZDQ3StsjO1J0Wg/exec?email=a.khoroshavin@innopolis.university&service=calculatePrice&plan=fixed_price&type=budget&inno_discount=yes&distance=1&time=1&planned_time=1 | Invalid Request | {"price":16} | ❌ |
| 15 | https://script.google.com/macros/s/AKfycbx8vetYY9K8yM3a46_mZJX2kMgbRgOEBb1_FlgYlMsOuPM5C5aw0FwTZDQ3StsjO1J0Wg/exec?email=a.khoroshavin@innopolis.university&service=calculatePrice&plan=fixed_price&type=budget&inno_discount=yes&distance=1&planned_distance=-1&time=1&planned_time=1 | Invalid Request | {"price":16} | ❌ |
| 16 | https://script.google.com/macros/s/AKfycbx8vetYY9K8yM3a46_mZJX2kMgbRgOEBb1_FlgYlMsOuPM5C5aw0FwTZDQ3StsjO1J0Wg/exec?email=a.khoroshavin@innopolis.university&service=calculatePrice&plan=fixed_price&type=budget&inno_discount=yes&distance=1&planned_distance=abc&time=1&planned_time=1 | Invalid Request |  {"price":16} | ❌ |
| 17 | https://script.google.com/macros/s/AKfycbx8vetYY9K8yM3a46_mZJX2kMgbRgOEBb1_FlgYlMsOuPM5C5aw0FwTZDQ3StsjO1J0Wg/exec?email=a.khoroshavin@innopolis.university&service=calculatePrice&plan=fixed_price&type=budget&inno_discount=no&distance=1&planned_distance=1&time=1 | Invalid Request | {"price":16.666666666666668} | ❌ |
| 18 | https://script.google.com/macros/s/AKfycbx8vetYY9K8yM3a46_mZJX2kMgbRgOEBb1_FlgYlMsOuPM5C5aw0FwTZDQ3StsjO1J0Wg/exec?email=a.khoroshavin@innopolis.university&service=calculatePrice&plan=fixed_price&type=budget&inno_discount=no&distance=1&planned_distance=1&time=1&planned_time=-1 | Invalid Request | Invalid Request | ✅ |
| 19 | https://script.google.com/macros/s/AKfycbx8vetYY9K8yM3a46_mZJX2kMgbRgOEBb1_FlgYlMsOuPM5C5aw0FwTZDQ3StsjO1J0Wg/exec?email=a.khoroshavin@innopolis.university&service=calculatePrice&plan=fixed_price&type=budget&inno_discount=no&distance=1&planned_distance=1&time=1&planned_time=abc | Invalid Request | {"price":16.666666666666668} | ❌ |
| 20 | https://script.google.com/macros/s/AKfycbx8vetYY9K8yM3a46_mZJX2kMgbRgOEBb1_FlgYlMsOuPM5C5aw0FwTZDQ3StsjO1J0Wg/exec?email=a.khoroshavin@innopolis.university&service=calculatePrice&plan=minute&type=budget&inno_discount=yes&time=1 | {"price":14.25} | {"price":14.399999999999999} | ❌ |
| 21 | https://script.google.com/macros/s/AKfycbx8vetYY9K8yM3a46_mZJX2kMgbRgOEBb1_FlgYlMsOuPM5C5aw0FwTZDQ3StsjO1J0Wg/exec?email=a.khoroshavin@innopolis.university&service=calculatePrice&plan=minute&type=budget&inno_discount=no&time=1 | {"price":15} | {"price":15} | ✅ |
| 22 | https://script.google.com/macros/s/AKfycbx8vetYY9K8yM3a46_mZJX2kMgbRgOEBb1_FlgYlMsOuPM5C5aw0FwTZDQ3StsjO1J0Wg/exec?email=a.khoroshavin@innopolis.university&service=calculatePrice&plan=minute&type=luxury&inno_discount=yes&time=1 | {"price":35.15} | {"price":35.519999999999996} | ❌ |
| 23 | https://script.google.com/macros/s/AKfycbx8vetYY9K8yM3a46_mZJX2kMgbRgOEBb1_FlgYlMsOuPM5C5aw0FwTZDQ3StsjO1J0Wg/exec?email=a.khoroshavin@innopolis.university&service=calculatePrice&plan=minute&type=luxury&inno_discount=no&time=1 | {"price":37} | {"price":37} | ✅ |
| 24 | https://script.google.com/macros/s/AKfycbx8vetYY9K8yM3a46_mZJX2kMgbRgOEBb1_FlgYlMsOuPM5C5aw0FwTZDQ3StsjO1J0Wg/exec?email=a.khoroshavin@innopolis.university&service=calculatePrice&plan=fixed_price&type=budget&inno_discount=yes&distance=1&planned_distance=1&time=1&planned_time=1 | {"price":13.3} | {"price":12} | ❌ |
| 25 | https://script.google.com/macros/s/AKfycbx8vetYY9K8yM3a46_mZJX2kMgbRgOEBb1_FlgYlMsOuPM5C5aw0FwTZDQ3StsjO1J0Wg/exec?email=a.khoroshavin@innopolis.university&service=calculatePrice&plan=fixed_price&type=budget&inno_discount=no&distance=1&planned_distance=1&time=1&planned_time=1 | {"price":14} | {"price":12.5} | ❌ |
| 26 | https://script.google.com/macros/s/AKfycbx8vetYY9K8yM3a46_mZJX2kMgbRgOEBb1_FlgYlMsOuPM5C5aw0FwTZDQ3StsjO1J0Wg/exec?email=a.khoroshavin@innopolis.university&service=calculatePrice&plan=fixed_price&type=budget&inno_discount=no&distance=94&planned_distance=100&time=100&planned_time=94 | {"price":1400} | {"price":1250} | ❌ |
| 27 | https://script.google.com/macros/s/AKfycbx8vetYY9K8yM3a46_mZJX2kMgbRgOEBb1_FlgYlMsOuPM5C5aw0FwTZDQ3StsjO1J0Wg/exec?email=a.khoroshavin@innopolis.university&service=calculatePrice&plan=fixed_price&type=budget&inno_discount=yes&distance=93&planned_distance=100&time=100&planned_time=100 | {"price":1425} | {"price":1200} | ❌ |
| 28 | https://script.google.com/macros/s/AKfycbx8vetYY9K8yM3a46_mZJX2kMgbRgOEBb1_FlgYlMsOuPM5C5aw0FwTZDQ3StsjO1J0Wg/exec?email=a.khoroshavin@innopolis.university&service=calculatePrice&plan=fixed_price&type=budget&inno_discount=no&distance=100&planned_distance=100&time=100&planned_time=93 | {"price":1500} | {"price":1250} | ❌ |

## Found bugs:
With the help of Decision table and after some manual tries with failed tests the following errors were found:
1) The actual value of `Fixed price per km` is 12.5, not 14 - this doesn't correspond to description _[Test cases 24, 25]_
2) The actual value of `Allowed deviations in %` is 8, not 6 - this doesn't correspond to description _[Test cases 27, 28]_
3) The actual value of `Inno discount in %` is 4, not 5 - this doesn't correspond to description _[Test cases 20, 22, 24, 27]_
4) Fields `time` and `planned_time` are not checked are they empty or not numberic - server must return `Invalid Request` but it doesn't _[Test cases 7, 9, 17, 19]_
5) Value 0 of field `time` doesn't cause domain error - `Invalid Request` must be returned _[Test case 8]_
6) The negative value of `planned_time` causes `Invalid Request` with `minute` plan while it should be ignored (`planned_time` doesn't affect calculations of `minute` plan cost) _[Manual experiments]_
7) The `fixed_price` plan is available for `luxury` cars as well - this doesn't correspond to description _[Test cases 10]_
8) Fields `distance` and `planned_distance` are not checked for any incorrect value - requests with empty, non-positive and not numberic values do not cause an error _[Test cases 11 - 16]_
9) In case of exceeding the deviation in `fixed_price` plan, the basic cost for time is 16.6, not 15 - this doesn't correspond to description _[Manual experiments while searching for actual value of `Allowed deviations in %`, also may be noticeable in test cases 17, 19]_
10) The typo in 2nd line of `getSpec` service - missed "g" in word "Budet" _[Just some attention in action ) ]_ 

## Assumptions
Due to uncompletness of requirements there are some questions that can affect on test coverage:
1) I assume that `inno_discount` field is required for any request. \
  If not, test cases 3 & 4 are irrelevant
2) I assume that `distance`, `planned_distance`, and `planned_time` fields should be ignored for `minute` tariff. \
  If not, then test cases 11 - 19 must be tested with any `plan` values
